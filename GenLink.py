from urllib.parse import urlencode
import pickle
from requests import Session
from bs4 import BeautifulSoup


def generateLink(session):
    # generating our link from each magazine
    l1 = 'https://thuglak.com/thuglak/archive.php?'
    # page 0 to 640
    link_list = []
    archive_title_list = []
    # Generating link for all the pages
    for x in range(0,640+20,20):
        query = {'argCnt':x}
        start_link = l1 + urlencode(query)
        dump = session.get(start_link)
        BsObj = BeautifulSoup(dump.text, 'lxml')
        l = BsObj.find('td').find_all('a')
        for y in l[0:-6]:
            archive_title_list.append(y.text.strip())
            if y['href'] != 'index1.php?cPath=1':
                link_list.append('https://thuglak.com/thuglak/'+y['href'])
    
    for x in range(0,640+20,20):
        query = {'argCnt':x}
        start_link = l1 + urlencode(query)
        try:
            link_list.remove(start_link)
        except ValueError as e:
            print(start_link + ' Not Present')

    with open("articles_link.txt","wb") as fp:
        pickle.dump(link_list,fp)  #Save links in a pickle file
    with open("articles_title.txt","wb") as fp:
        pickle.dump(archive_title_list,fp)  #Save title in a pickle file


# Login
login_url = "https://thuglak.com/thuglak/login.php?action=progress"
data = {
    'email_address':'editorkb@gmail.com',
    'password':'thuglakpassword'
    }
session = Session()
session.post(url=login_url, data=data)

generateLink(session)
